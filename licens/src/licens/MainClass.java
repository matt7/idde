package licens;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class MainClass {

	public static HashMap<String, Diak> createDictionary() {
		HashMap<String, Diak> dict = new HashMap<String, Diak>();
		dict.put("Ionescu", new Diak("Ionescu"));
		OsztondijasDiak diak2 = new OsztondijasDiak("Popescu", 200);
		dict.put(diak2.getName(), diak2);
		return dict;
	}

	public static boolean ellenoriz(HashMap<String, Diak> dict, char c) {
		for (String nev : dict.keySet()) {
			if (nev.charAt(0) == c)
				return true;
		}
		return false;
	}

	public static List<Integer> primes() {

		List<Integer> primenumbers = new ArrayList<Integer>();
		primenumbers.add(2); // single even prime
		for (int i = 3; i <= 100; i += 2) { // testing just the odd numbers
			if (isprime(i)) {
				primenumbers.add(i);
			}
		}
		return primenumbers;
	}

	private static boolean isprime(int n) {

		if (n % 2 == 0) {
			return false;
		}
		int nh = n / 2;
		for (int d = 3; d < nh; d += 2) {
			if (n % d == 0) {
				return false;
			}
		}
		return true;
	}

	public static int[] insertionSort(int[] array) {
		int len = array.length;
		int val;
		for (int i = 1; i < len; i++) {
			val = array[i];
			int j = 0;
			while ((array[j] < val) && (j < i)) { // searching for the palace in the array
				j++;
			}
			for (int k = i; k > j; k--) { // moving the elements
				array[k] = array[k - 1];
			}
			array[j] = val;
		}
		return array;
	}

	public static void main(String[] args) {

		List<Integer> pr = primes();
		for (Integer integer : pr) {
			// System.out.print(integer + " ");
		}
		int[] a = { 4, 3, 11,5, 8, 1 };
		System.out.println();
		a = insertionSort(a);
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}

		//
		// HashMap<String, Diak> dict = createDictionary();
		// System.out.println("Read chars:");
		// Scanner in = new Scanner(System.in);
		// System.console().readLine();
		// String karakters = in.nextLine();
		// in.close();
		// boolean found = false;
		// for (String key : dict.keySet()) {
		// if (key.equals(karakters)) {
		// System.out.println(dict.get(key));
		// found = true;
		// }
		// }
		// if (!found)
		// System.out.println("nem talalt");
		//
		// if (ellenoriz(dict, karakters.charAt(0))) {
		// System.out.println("Found");
		// } else
		// System.out.println("Not found");
	}

}

package licens;

public class Diak {

	private String name;
	
	public Diak(String name){
		this.name=name;	
	}
	
	@Override
	public String toString(){
		return name;
	}
	
	public String getName(){
		return name;
	}
}

package edu.ubbcluj.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.eclipse.jface.viewers.ColumnLabelProvider;

import edu.ubbcluj.pojo.Track;

public class MyLabelProvider extends ColumnLabelProvider  {
	
	private Field columnField;
	
	public MyLabelProvider(Field f) {
		columnField=f;
	}
	
	@Override
	public String getText(Object element) {
		Track t = (Track) element;
		try {
		PropertyDescriptor pd = new PropertyDescriptor(columnField.getName(),
				Track.class);
		Method m = pd.getReadMethod();
		Class<?> type = columnField.getType();
	
			if (type != java.lang.String.class) {
				return 	""+ m.invoke(element);
			} else {
				return 	 (String) m.invoke(element);
			}	
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	return "";
		
		
	}
}

package edu.ubbcluj.view;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;





import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.part.ViewPart;






import edu.ubbcluj.daofactory.TrackDAO;
import edu.ubbcluj.pojo.Track;
import edu.ubbcluj.util.MyLabelProvider;

public class ViewPart1 extends ViewPart {

	public ViewPart1() {
		// TODO Auto-generated constructor stub
	}

	public void createPartControl(Composite parent) {
		// TODO Auto-generated method stub

		TableViewer viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

	
		// viewer.setInput(tl);
		createColumns(viewer);
		final Table table = viewer.getTable();
	
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

	}

	private void createColumns(TableViewer viewer) {
		TrackDAO td = TrackDAO.getTrackDAO();
		List<Track> tl = td.getAllDataRows();
		ArrayList<Field> fields = td.getFields();
		for (Field field : fields){
			TableViewerColumn col = createTableViewerColumn(viewer ,field.getName(), 120, fields.indexOf(field));
			col.setLabelProvider(new MyLabelProvider(field));		
		}
		viewer.setContentProvider(new ArrayContentProvider());
		viewer.setInput(tl);

	}

	private TableViewerColumn createTableViewerColumn(TableViewer viewer,
			String label, int i, int indexOf) {
		TableViewerColumn viewerColumn = new TableViewerColumn(viewer,
				SWT.NONE);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(label);
		column.setWidth(i);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	}

	public void setFocus() {
		// TODO Auto-generated method stub

	}

}

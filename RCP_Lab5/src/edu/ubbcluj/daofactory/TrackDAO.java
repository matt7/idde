package edu.ubbcluj.daofactory;

import edu.ubbcluj.dao.GenericDAOImpl;
import edu.ubbcluj.pojo.Track;

public class TrackDAO extends GenericDAOImpl<Track>{

	//http://gerardnico.com/wiki/language/java/dao
	private TrackDAO() {
		super(Track.class);
		// TODO Auto-generated constructor stub
	}
	
	public static TrackDAO getTrackDAO(){
		return new TrackDAO();
	}	
}

package edu.ubbcluj.dao;


import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import edu.ubbcluj.util.PropertyLoader;

public class GenericDAOImpl<T> implements GenericDAOInterface<T> {

	private Class<T> myClass;
	private String connString;
	private String uname, pword;

	public GenericDAOImpl(final Class<T> pclass) {
		this.myClass = pclass;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (Exception e) {
			e.printStackTrace();
		}
		connString = PropertyLoader.getProperty("url");
		uname = PropertyLoader.getProperty("username");
		pword = PropertyLoader.getProperty("password");
		createTableIfNotExists();
	}

	private void createTableIfNotExists() {
		// TODO Auto-generated method stub
		String createQuery = "CREATE TABLE IF NOT EXISTS "
				+ myClass.getSimpleName() + " (";
		for (Field f : myClass.getDeclaredFields()) {

			Class<?> type = f.getType();
			if (type != java.lang.String.class) {
				createQuery += f.getName() + " INT(64)";
				if (f.getName().equals("id")) {
					createQuery += " NOT NULL PRIMARY KEY AUTO_INCREMENT ";
				}
			} else {
				createQuery += f.getName() + " VARCHAR(255)";
			}
			createQuery += ",";
		}
		createQuery = createQuery.substring(0, createQuery.length() - 1);
		createQuery += ")";
		System.out.println(createQuery);
		try {
			Connection con = DriverManager.getConnection(connString, uname,
					pword);
			Statement s = con.createStatement();
			s.execute(createQuery);
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<T> getAllDataRows() {

		List<T> res;
		ResultSet r;

		try {
			Connection con = DriverManager.getConnection(connString, uname,
					pword);
			Statement s = con.createStatement();
			r = s.executeQuery("SELECT * FROM " + myClass.getSimpleName());
			res = createList(r);
			con.close();
			return res;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public void insertObjects(List<T> objects) {
		String insq;
		for (T obj : objects) {

			try {
				insq = "INSERT INTO " + myClass.getSimpleName() + " ";
				// Class objclass = obj.getClass();

				Connection con = DriverManager.getConnection(connString, uname,
						pword);
				Field[] fields = myClass.getDeclaredFields();
				String fs = "(";
				String qmarks = "(";
				for (Field f : fields) {
					if (!f.getName().equals("id")) {
						fs += f.getName() + ",";
						qmarks += "?,";
					}
				}
				fs = fs.substring(0, fs.length() - 1);
				qmarks = qmarks.substring(0, qmarks.length() - 1);
				fs += ")";
				qmarks += ")";
				insq += fs + " VALUES " + qmarks;
				// System.out.println(insq);
				java.sql.PreparedStatement pstmt = con.prepareStatement(insq);
				for (int i = 0; i < fields.length; i++) {
					Field f = fields[i];
					Class<?> type = f.getType();
					PropertyDescriptor pd = new PropertyDescriptor(f.getName(),
							myClass);
					Method method = pd.getReadMethod();
					if (type != java.lang.String.class) {
						if (!f.getName().equals("id")) {
							pstmt.setInt(i, (Integer) method.invoke(obj));
						}
					} else {
						pstmt.setString(i, (String) method.invoke(obj));
					}

				}
				System.out.println(pstmt.toString());
				pstmt.execute();
				con.close();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	public void updateObject(T obj) {

		String insq;

		try {
			insq = "UPDATE " + myClass.getSimpleName() + " SET ";
			// Class objclass = obj.getClass();

			Connection con = DriverManager.getConnection(connString, uname,
					pword);
			Field[] fields = myClass.getDeclaredFields();
			String fs = "";
			for (Field f : fields) {
				if (!f.getName().equals("id")) {
					fs += f.getName() + " = ? ,";
				}
			}
			fs = fs.substring(0, fs.length() - 1);
			fs += " WHERE id = ?";
			insq += fs;
			// System.out.println(insq);
			java.sql.PreparedStatement pstmt = con.prepareStatement(insq);
			for (int i = 0; i < fields.length; i++) {
				Field f = fields[i];
				Class<?> type = f.getType();
				PropertyDescriptor pd = new PropertyDescriptor(f.getName(),
						myClass);
				Method method = pd.getReadMethod();
				if (type != java.lang.String.class) {
					if (!f.getName().equals("id")) {
						// System.out.println("NotString: " +
						// method.invoke(obj));
						pstmt.setInt(i, (Integer) method.invoke(obj));
					} else {
						pstmt.setInt(fields.length,
								(Integer) method.invoke(obj));
					}
				} else {
					// System.out.println("String: " + method.invoke(obj));
					pstmt.setString(i, (String) method.invoke(obj));
				}

			}
			System.out.println(pstmt.toString());
			pstmt.execute();
			con.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void deleteObject(T obj) {
		
		String insq;	

			try {
				insq = "DELETE FROM " + myClass.getSimpleName() + " WHERE id=? ";

				Connection con = DriverManager.getConnection(connString, uname,
						pword);
				java.sql.PreparedStatement pstmt = con.prepareStatement(insq);
				Field[] fields = myClass.getDeclaredFields();
				for (Field field : fields) {
					if (field.getName().equals("id")) {
						PropertyDescriptor pd = new PropertyDescriptor(field.getName(),
								myClass);
						Method method = pd.getReadMethod();
						pstmt.setInt(1, (Integer) method.invoke(obj));
					}
				}
				System.out.println(pstmt.toString());
				pstmt.execute();
				con.close();
				
				
			}catch(Exception e){
				e.printStackTrace();
			}
		
	}

	private List<T> createList(ResultSet r) {

		List<T> res = new ArrayList<T>();
		try {
			while (r.next()) {

				T newi = myClass.newInstance();
				for (Field f : myClass.getDeclaredFields()) {
					Object result = r.getObject(f.getName());
					PropertyDescriptor pd = new PropertyDescriptor(f.getName(),
							myClass);
					Method m = pd.getWriteMethod();
					Class<?> type = f.getType();
					if (type != java.lang.String.class) {
						m.invoke(newi, Integer.parseInt(result + ""));
					} else {
						m.invoke(newi, result);
					}	
				}
				res.add(newi);
			}
			return res;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return res;
	}
	
	public ArrayList<Field> getFields(){
		ArrayList<Field> l=new ArrayList<Field>();
		for (Field f : myClass.getDeclaredFields()) {
			l.add(f);
		}
		
		return l;
		
	}


}

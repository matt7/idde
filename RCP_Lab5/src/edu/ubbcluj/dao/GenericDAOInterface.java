package edu.ubbcluj.dao;

import java.util.List;

public interface GenericDAOInterface<T> {
	
	List<T> getAllDataRows();
	void insertObjects(List<T> objects );
	void updateObject( T obj );
	void deleteObject( T obj );

}

package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import edu.ubbcluj.simplexsolver.model.InitSimplexTable;
import edu.ubbcluj.simplexsolver.model.SimplexTable;
import javax.swing.text.StyledEditorKit.ForegroundAction;
import java.util.ArrayList;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//HU\" \"http://www.w3.org/TR/html4/loose.dtd\">\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<title>Simplex Method</title>\n");
      out.write("<!-- Latest compiled and minified CSS -->\n");
      out.write("<link rel=\"stylesheet\"\n");
      out.write("\thref=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css\">\n");
      out.write("\n");
      out.write("<!-- Optional theme -->\n");
      out.write("<link rel=\"stylesheet\"\n");
      out.write("\thref=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css\">\n");
      out.write("\n");
      out.write("<!-- Latest compiled and minified JavaScript -->\n");
      out.write("<script\n");
      out.write("\tsrc=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js\"></script>\n");
      out.write("</head>\n");
      out.write("<body>\n");
      out.write("\n");
      out.write("\t<div class=\"container col-xs-4\">\n");
      out.write("\t<h2>Rendszer</h2>\n");
      out.write("\t\t");

			InitSimplexTable it= (InitSimplexTable) request.getAttribute("inittable");

			if(it!=null){
		
      out.write("<table class=\"table table-condensed table-bordered\">\n");
      out.write("\t\t\t<thead>\n");
      out.write("\t\t\t\t");

					for(int i=0;i<it.n;i++){
				
      out.write("\n");
      out.write("\t\t\t\t");
      out.print("<th>A" + (i+1) + "</th>");
      out.write("\n");
      out.write("\t\t\t\t");

					}
				
      out.write("\n");
      out.write("\t\t\t\t<th>b</th>\n");
      out.write("\t\t\t</thead>\n");
      out.write("\t\t\t");

				for(int i=0;i<it.m;i++){
			
      out.write("\n");
      out.write("\t\t\t<tr>\n");
      out.write("\t\t\t\t");

					for(int j=0;j<it.n;j++){
				
      out.write("\n");
      out.write("\t\t\t\t");
      out.print("<td>" + it.koeffmatrix[i][j] + "</td>");
      out.write("\n");
      out.write("\t\t\t\t");

					}
				
      out.write("\n");
      out.write("\t\t\t\t");
      out.print("<td>="+it.szabadtagvekt[i]+"</td></tr>");
      out.write("\n");
      out.write("\t\t\t\t");

					}
				
      out.write("\n");
      out.write("\t\t\t\t");
      out.print("<tr>");
      out.write("\n");
      out.write("\t\t\t\t");

					for(int j=0;j<it.n;j++){
				
      out.write("\n");
      out.write("\t\t\t\t");
      out.print("<td>"+it.celfgv[j]);
      out.print("</td>");
      out.write("\n");
      out.write("\t\t\t\t");

					}
				
      out.write("<td>-> MIN</td>\n");
      out.write("\t\t\t</tr>\n");
      out.write("\t\t</table>\n");
      out.write("\t</div>\n");
      out.write("\t<div class=\"container col-xs-4\">\n");
      out.write("\t\t<h2>Tables</h2>\n");
      out.write("\t\t");

			}
			
			ArrayList<SimplexTable> tables=(ArrayList<SimplexTable>) request.getAttribute("tables");
			if(tables!=null){
				for(SimplexTable table : tables){
		
      out.write("\n");
      out.write("\t\t<table class=\"table table-condensed table-bordered\">\n");
      out.write("\t\t\t<thead>\n");
      out.write("\n");
      out.write("\t\t\t\t");
      out.print("<th>#"+tables.indexOf(table)+"</th>");
      out.write("\n");
      out.write("\n");
      out.write("\t\t\t\t");

					for(int i=0;i<it.m;i++){
				
      out.write("\n");
      out.write("\t\t\t\t");
      out.print("<th>"+table.bazisvektorok.get(i)+" | "+table.bvekt[i]+"</th>");
      out.write("\n");
      out.write("\t\t\t\t");

					}
				
      out.write("\n");
      out.write("\n");
      out.write("\t\t\t\t<th>d</th>\n");
      out.write("\t\t\t</thead>\n");
      out.write("\t\t\t");

					for(int i=0;i<it.n-it.m;i++){
				
      out.write("\n");
      out.write("\t\t\t\t");
      out.print("<tr><td>"+table.nembazisvektorok.get(i)+" | "+table.nbvekt[i]+"</td>");
      out.write("\n");
      out.write("\t\t\t\t");

				for(int j=0;j<it.m;j++){
					
      out.write("\n");
      out.write("\t\t\t\t\t");
      out.print("<td>"+table.alfa[i][j]+"</td>");
      out.write("\n");
      out.write("\t\t\t\t\t");
		
				}
				
      out.write("\n");
      out.write("\t\t\t\t");
      out.print("<td>"+table.d[i]+"</td>");
      out.write("\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t\t");

					}
				
      out.write("\n");
      out.write("\t\t\t\t<tr>\n");
      out.write("\t\t\t\t<td>X</td>\n");
      out.write("\t\t\t\t");

				for(int j=0;j<it.m;j++){
					
      out.write("\n");
      out.write("\t\t\t\t\t");
      out.print("<td>"+table.xb[j]+"</td>");
      out.write("\n");
      out.write("\t\t\t\t\t");
		
				}
				
      out.write("\n");
      out.write("\t\t\t\t");
      out.print("<td>c|x "+table.cx+"</td>");
      out.write("\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t\t<tr>\n");
      out.write("\t\t\t\t<td>X/Lij</td>\n");
      out.write("\t\t\t\t");
if(!table.optimalis)
				for(int j=0;j<it.m;j++){
					
      out.write("\n");
      out.write("\t\t\t\t\t");
      out.print("<td>"+table.xl[j]+"</td>");
      out.write("\n");
      out.write("\t\t\t\t\t");
		
				}
				
      out.write("\n");
      out.write("\t\t\t\t");
      out.print("<td>Optimalis:"+table.optimalis+"</td>");
      out.write("\n");
      out.write("\t\t\t\t</tr>\n");
      out.write("\t\t\t\t\n");
      out.write("\t\t</table>\n");
      out.write("\n");
      out.write("\t\t");

			}
				}
		
      out.write("\n");
      out.write("\t</div>\n");
      out.write("\t<div class=\"container col-xs-4\">\n");
      out.write("\t\t<h3>File Upload:</h3>\n");
      out.write("\n");
      out.write("\t\t<form action=\"HomeServlet\" method=\"post\" enctype=\"multipart/form-data\">\n");
      out.write("\t\t\tSelect a file to upload: <input type=\"file\" name=\"file\"\n");
      out.write("\t\t\t\taccept=\"*.txt\" /> <br /> <input type=\"submit\" value=\"Upload File\" />\n");
      out.write("\t\t</form>\n");
      out.write("\t\tBemeneti file formatum: <br> 1.sor: vektorok szama, <br>\n");
      out.write("\t\t2.sor: vektorok hossza, <br> 3.sor:celfgvegyutthatok, <br>\n");
      out.write("\t\t4-?sor: matrix, <br> utolso:szabadtagvektor <br> Elvalaszto\n");
      out.write("\t\tkarakter vesszo. <br>\n");
      out.write("\t</div>\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}

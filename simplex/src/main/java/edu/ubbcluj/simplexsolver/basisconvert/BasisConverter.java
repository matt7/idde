package edu.ubbcluj.simplexsolver.basisconvert;

import edu.ubbcluj.simplexsolver.model.Rational;

public class BasisConverter {

	public static Rational[] changeBasis(Rational[][] basis, Rational[] vector) {
		// TODO : Need to check if the pivot element is zero or not
		int n = vector.length;
		for (int i = 0; i < n; i++) {
			// Rational pivot = basis[i][i];
			Rational rec = basis[i][i].reciprocal();
			for (int j = 0; j < n; j++) {// elosztjuk az egyenletet
				basis[i][j] = basis[i][j].times(rec);
			}
			vector[i] = vector[i].times(rec);
			for (int j = 0; j < n; j++) {//
				if (i == j)
					continue;
				Rational neg = basis[j][i].negate();
				for (int k = 0; k < n; k++) {
					basis[j][k] = basis[j][k].plus(basis[i][k].times(neg));
				}
				vector[j] = vector[j].plus(vector[i].times(neg));
			}
			for (int l = 0; l < vector.length; l++) {
				for (int j = 0; j < vector.length; j++) {
					System.out.print(basis[l][j] + " ");
				}
				System.out.println(vector[l] + "");
			}
			System.out.println("");
		}

		return vector;
	}

	public static void main(String argv[]) throws Exception {
		//int a[][] = { { 1, 1, 1 }, { 4, 3, 2 }, { 2, 1, 1 } };
		//long b[] = { 1, -2, -2 };
		
		int a[][] = { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
		long b[] = { -1, 1, 1 };
		
		Rational basis[][] = new Rational[3][3];
		Rational vector[] = new Rational[3];
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a.length; j++) {
				basis[i][j] = new Rational((long) a[i][j], 1L);
			}
			vector[i] = new Rational(b[i], 1L);
		}

		vector = BasisConverter.changeBasis(basis, vector);
		for (int i = 0; i < vector.length; i++) {
			System.out.println(vector[i] + " ");
		}

		// throw new Exception("asd");
	}

}

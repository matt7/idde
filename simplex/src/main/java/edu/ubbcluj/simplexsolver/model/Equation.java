package edu.ubbcluj.simplexsolver.model;

import java.util.ArrayList;


public class Equation extends ArrayList<Double>{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Equation substract(Equation e){
		Equation eq=new Equation();
		Double d=0.0;
		for (int i = 0; i < e.size(); i++) {
			d=this.get(i)-e.get(i);
			eq.set(i, d);
		}
		return eq;
		
	}
}

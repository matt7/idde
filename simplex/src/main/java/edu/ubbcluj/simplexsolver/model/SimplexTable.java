package edu.ubbcluj.simplexsolver.model;

import java.util.ArrayList;

public class SimplexTable implements SimplexInterface {

	public ArrayList<String> bazisvektorok = new ArrayList<String>();
	public Rational[] bvekt;
	public ArrayList<String> nembazisvektorok = new ArrayList<String>();
	public Rational[] nbvekt;
	public Rational[][] alfa;
	public Rational[] d;
	public Rational[] xb;
	public Rational[] xl;
	public Rational cx;
	public boolean optimalis = true;
	public boolean megoldhato = true;
	public Pivot p;
	int m, n;

	public SimplexTable(SimplexTable initTable) {
		m = initTable.m;
		n = initTable.n;
		nembazisvektorok = new ArrayList<String>(initTable.nembazisvektorok);
		bazisvektorok = new ArrayList<String>(initTable.bazisvektorok);
		bvekt = initTable.bvekt.clone();
		nbvekt = initTable.nbvekt.clone();
		alfa = new Rational[n - m][m];
		d = new Rational[n - m];
		xb = new Rational[m];
		xl = new Rational[m];
		cx = new Rational(0, 1);

		String st = nembazisvektorok.get(initTable.p.i);
		Rational nemb = nbvekt[initTable.p.i];
		nembazisvektorok.set(initTable.p.i, bazisvektorok.get(initTable.p.j));
		nbvekt[initTable.p.i] = bvekt[initTable.p.j];
		bvekt[initTable.p.j] = nemb;
		bazisvektorok.set(initTable.p.j, st);

		
		int pi = initTable.p.i;
		int pj = initTable.p.j;
		for (int i = 0; i < n - m; i++) {
			if (i == initTable.p.i) {
				alfa[i][pj] = initTable.alfa[i][pj].reciprocal();
			} else {
				alfa[i][pj] = initTable.alfa[i][pj]
						.dividedBy(initTable.alfa[initTable.p.i][initTable.p.j]);
			}
		}

		for (int i = 0; i < m; i++) {
			if (i != initTable.p.j) {
				alfa[pi][i] = initTable.alfa[pi][i]
						.dividedBy(initTable.alfa[pi][pj]
								.negate());
			}
		}

		for (int i = 0; i < alfa.length; i++) {
			if (i == pi){continue;}
				
			for (int k = 0; k < alfa[i].length; k++) {
				if (pj == k)
					continue;
				alfa[i][k] = ((initTable.alfa[i][k]
						.times(initTable.alfa[pi][pj]))
						.minus(initTable.alfa[pi][k]
								.times(initTable.alfa[i][pj])))
						.dividedBy(initTable.alfa[pi][pj]);
			}
		}
		
		for (int i = 0; i < xb.length; i++) {
			xb[i] = ((initTable.xb[i]
					.times(initTable.alfa[pi][pj]))
					.minus(initTable.alfa[pi][i]
							.times(initTable.xb[pj])))
					.dividedBy(initTable.alfa[pi][pj]);
		}
		xb[pj]=initTable.xb[pj].dividedBy(initTable.alfa[pi][pj]);
		
		cx=new Rational(0,1);
		for (int i = 0; i < m; i++) {
			cx=cx.plus(bvekt[i].times(xb[i]));
		}
		
		Rational s= new Rational(0,1);
		for (int i = 0; i < n - m; i++) {
			for (int j = 0; j < m; j++) {
				s=s.plus(alfa[i][j].times(bvekt[j]));
			}
			d[i]=s.minus(nbvekt[i]);
			s = new Rational(0,1);
		}
		
		this.checkOptim();
	}

	public SimplexTable() {
		// TODO Auto-generated constructor stub
	}

	public void checkOptim() {
		for (int i = 0; i < d.length; i++) {
			optimalis = optimalis && (d[i].compareTo(new Rational(0, 1)) != 1);
		}

	}

	public SimplexTable optimize() {
		megoldhato = checkMego();
		if (megoldhato) {
			selectPivot();
			return new SimplexTable(this);
		}

		return null;
	}

	private boolean checkMego() {
		for (int i = 0; i < d.length; i++) {
			if (d[i].compareTo(new Rational(0, 1)) == 1) {
				if (justNeg(i)) {
					return false;
				}
			}
		}
		return true;
	}

	private boolean justNeg(int row) {
		for (int i = 0; i < m; i++) {
			if (alfa[row][i].compareTo(new Rational(0, 1)) == 1) {
				return false;
			}
		}
		return true;
	}

	private void selectPivot() {
		p = new Pivot();
		Rational max = d[0];
		int maxind = 0;
		for (int i = 1; i < d.length; i++) {
			if (d[maxind].compareTo(d[i]) == -1) {
				maxind = i;
			}
		}
		p.i = maxind;
		xl = new Rational[m];
		for (int i = 0; i < alfa.length; i++) {
			try {
				if (alfa[maxind][i].compareTo(new Rational(0,1))==-1) {
					xl[i] = new Rational(10000, 1);
				} else {

					xl[i] = xb[i].dividedBy(alfa[maxind][i]);
				}
			} catch (Exception e) {
				xl[i] = new Rational(10000, 1);
			}
		}

		int minind = 0;
		for (int i = 1; i < xl.length; i++) {
			if (xl[minind].compareTo(xl[i]) == 1
					&& xl[i].compareTo(new Rational(0, 1)) == 1) {
				minind = i;
			}
		}

		p.j = minind;
		System.out.println(p.i + " " + p.j);
	}

}

package edu.ubbcluj.simplexsolver.model;


import edu.ubbcluj.simplexsolver.basisconvert.BasisConverter;

public class InitSimplexTable implements SimplexInterface {

	public Rational[][] koeffmatrix;
	public Rational[] szabadtagvekt;
	public Rational[] celfgv;
	public Rational[] kontroll;
	public Rational celfgvertek;
	public Pivot pivotelem;
	public int m, n;

	public InitSimplexTable() {

	}

	public SimplexTable getFirstTable() {

		SimplexTable f = new SimplexTable();
		int c = 0;
		f.m=m;
		f.n=n;
		Rational basis[][] = new Rational[m][m];
		f.bvekt = new Rational[m];
		f.nbvekt = new Rational[n - m];
		f.xb = new Rational[m];
		for (int i = n - m; i < n; i++) {
			f.bazisvektorok.add( "A" + (i + 1));
			f.bvekt[c] = celfgv[i];	
			for (int j = 0; j < m; j++) {
				basis[j][c] = koeffmatrix[j][i];
			}
			f.xb[c]=szabadtagvekt[c];
			c++;
		}
		f.alfa = new Rational[n - m][m];

		for (int i = 0; i < n - m; i++) {
			f.nembazisvektorok.add( "A" + (i + 1));
			f.nbvekt[i] = celfgv[i];
			Rational vect[] = new Rational[m];
			for (int j = 0; j < m; j++) {
				vect[j] = koeffmatrix[j][i];
			}
			f.alfa[i]=BasisConverter.changeBasis(basis,vect);
		}
		
		f.d=new Rational[n-m];
		Rational s= new Rational(0,1);
		for (int i = 0; i < n - m; i++) {
			for (int j = 0; j < m; j++) {
				s=s.plus(f.alfa[i][j].times(f.bvekt[j]));
			}
			f.d[i]=s.minus(f.nbvekt[i]);
			s = new Rational(0,1);
		}
		
		f.cx=new Rational(0,1);
		for (int i = 0; i < m; i++) {
			f.cx=f.cx.plus(f.bvekt[i].times(f.xb[i]));
		}
			

//		for (int i = 0; i < f.alfa.length; i++) {
//			for (int j = 0; j < f.alfa[i].length; j++) {
//				System.out.print(f.alfa[i][j]+" ");
//			}
//			System.out.println();
//		}
		
		return f;
	}


}

package edu.ubbcluj.simplexsolver.controller;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.servlet.http.Part;

import edu.ubbcluj.simplexsolver.model.InitSimplexTable;
import edu.ubbcluj.simplexsolver.model.Rational;
import edu.ubbcluj.simplexsolver.model.SimplexTable;

public class SimplexSolver {

	public InitSimplexTable initTable;
	public ArrayList<SimplexTable> tables;
	public ArrayList<String> lines = new ArrayList<String>();

	public SimplexSolver(Part filePart) throws Exception {
		InputStream fileContent = filePart.getInputStream();

		BufferedReader in = new BufferedReader(new InputStreamReader(
				fileContent));
		String line = null;

		StringBuilder ct = new StringBuilder();
		while ((line = in.readLine()) != null) {
			lines.add(line);
			ct.append(line);
			System.out.println(line);
		}
		initTable=new InitSimplexTable();
		createInitTable();
		fillFirstTable();
		SimplexTable lasttable=tables.get(tables.size()-1);
	/*	if(!lasttable.optimalis){
			SimplexTable nexttable = lasttable.optimize();
			tables.add(nexttable);
			lasttable=tables.get(tables.size()-1);
		}*/
		
		while(!lasttable.optimalis){
			SimplexTable nexttable = lasttable.optimize();
			tables.add(nexttable);
			lasttable=tables.get(tables.size()-1);
		}
		//tables= new ArrayList<InitSimplexTable>();
		//tables.add(initTable);
	}

	private void fillFirstTable() {
		SimplexTable first = initTable.getFirstTable();
		first.checkOptim();
		tables=new ArrayList<SimplexTable>();
		tables.add(first);
	}

	private void createInitTable() {
		
		int n=Integer.parseInt(lines.get(0));
		int m=Integer.parseInt(lines.get(1));
		initTable.n=n;
		initTable.m=m;
		initTable.koeffmatrix=new Rational[m][n];
		initTable.celfgv=new Rational[n];
		initTable.szabadtagvekt=new Rational[m];
		
		String line=lines.get(2);
		String nums[]=line.split(",");
		for (int j = 0; j < nums.length; j++) {
			initTable.celfgv[j] =  new Rational(Long.parseLong(nums[j]),1L);
		}
		
		for (int i = 0; i < m; i++) {
			line=lines.get(i+3);
			nums=line.split(",");
			for (int j = 0; j < nums.length; j++) {
				initTable.koeffmatrix[i][j] =  new Rational(Long.parseLong(nums[j]),1L);
			}
		}
		line=lines.get(m+3);
		nums=line.split(",");
		for (int j = 0; j < nums.length; j++) {
			initTable.szabadtagvekt[j] =  new Rational(Long.parseLong(nums[j]),1L);
		}			
	}
	
	
	
/*
	public static void main(String[] args) {

		File f = new File("src/main/resources/testval.txt");
		FileReader fr;
		try {
			fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);
			String s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}
			br.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
*/

}

package edu.ubbcluj.simplexsolver;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.fileupload.servlet.ServletFileUpload;


import edu.ubbcluj.simplexsolver.controller.SimplexSolver;


/**
 * Servlet implementation class HomeServlet
 */
@MultipartConfig
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HomeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		RequestDispatcher rd=request.getRequestDispatcher("index.jsp");	
		rd.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		 boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		 
		 if(isMultipart){

			 Part filePart = request.getPart("file");
			 try {
				SimplexSolver ss=new SimplexSolver(filePart);
				request.setAttribute("tables", ss.tables);
				request.setAttribute("inittable", ss.initTable);
			 
			 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}		  
		 }
		 RequestDispatcher rd=request.getRequestDispatcher("index.jsp");	
			rd.forward(request, response);
	}

}

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//HU" "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="edu.ubbcluj.simplexsolver.model.InitSimplexTable"%>
<%@page import="edu.ubbcluj.simplexsolver.model.SimplexTable"%>
<%@page import="javax.swing.text.StyledEditorKit.ForegroundAction"%>
<%@page import="java.util.ArrayList"%>
<html>
<head>
<title>Simplex Method</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

<!-- Optional theme -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">

<!-- Latest compiled and minified JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<body>

	<div class="container col-xs-4">
	<h2>Rendszer</h2>
		<%
			InitSimplexTable it= (InitSimplexTable) request.getAttribute("inittable");

			if(it!=null){
		%><table class="table table-condensed table-bordered">
			<thead>
				<%
					for(int i=0;i<it.n;i++){
				%>
				<%="<th>A" + (i+1) + "</th>"%>
				<%
					}
				%>
				<th>b</th>
			</thead>
			<%
				for(int i=0;i<it.m;i++){
			%>
			<tr>
				<%
					for(int j=0;j<it.n;j++){
				%>
				<%="<td>" + it.koeffmatrix[i][j] + "</td>"%>
				<%
					}
				%>
				<%="<td>="+it.szabadtagvekt[i]+"</td></tr>"%>
				<%
					}
				%>
				<%="<tr>"%>
				<%
					for(int j=0;j<it.n;j++){
				%>
				<%="<td>"+it.celfgv[j]%><%="</td>"%>
				<%
					}
				%><td>-> MIN</td>
			</tr>
		</table>
	</div>
	<div class="container col-xs-4">
		<h2>Tables</h2>
		<%
			}
			
			ArrayList<SimplexTable> tables=(ArrayList<SimplexTable>) request.getAttribute("tables");
			if(tables!=null){
				for(SimplexTable table : tables){
		%>
		<table class="table table-condensed table-bordered">
			<thead>

				<%="<th>#"+tables.indexOf(table)+"</th>"%>

				<%
					for(int i=0;i<it.m;i++){
				%>
				<%="<th>"+table.bazisvektorok.get(i)+" | "+table.bvekt[i]+"</th>"%>
				<%
					}
				%>

				<th>d</th>
			</thead>
			<%
					for(int i=0;i<it.n-it.m;i++){
				%>
				<%="<tr><td>"+table.nembazisvektorok.get(i)+" | "+table.nbvekt[i]+"</td>"%>
				<%
				for(int j=0;j<it.m;j++){
					%>
					<%="<td>"+table.alfa[i][j]+"</td>"%>
					<%		
				}
				%>
				<%="<td>"+table.d[i]+"</td>"%>
					
				
				</tr>
				<%
					}
				%>
				<tr>
				<td>X</td>
				<%
				for(int j=0;j<it.m;j++){
					%>
					<%="<td>"+table.xb[j]+"</td>"%>
					<%		
				}
				%>
				<%="<td>c|x "+table.cx+"</td>"%>
				</tr>
				
					<tr>
				<td>X/Lij</td>
				<%if(!table.optimalis)
				for(int j=0;j<it.m;j++){
					%>
					<%="<td>"+table.xl[j]+"</td>"%>
					<%		
				}
				%>
				<%="<td>Optimalis:"+table.optimalis+"</td>"%>
				</tr>
				
		</table>

		<%
			}
				}
		%>
	</div>
	<div class="container col-xs-4">
		<h3>File Upload:</h3>

		<form action="HomeServlet" method="post" enctype="multipart/form-data">
			Select a file to upload: <input type="file" name="file"
				accept="*.txt" /> <br /> <input type="submit" value="Upload File" />
		</form>
		Bemeneti file formatum: <br> 1.sor: vektorok szama, <br>
		2.sor: vektorok hossza, <br> 3.sor:celfgvegyutthatok, <br>
		4-?sor: matrix, <br> utolso:szabadtagvektor <br> Elvalaszto
		karakter vesszo. <br>
	</div>
</body>
</html>

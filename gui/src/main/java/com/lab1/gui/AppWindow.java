package com.lab1.gui;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;

import com.lab1.dbconnect.DatabaseAccess;

public class AppWindow extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JButton getButton = new JButton("Get Items");
	private JTable contentTable = new JTable();
	private DatabaseAccess dbAccess;

	public AppWindow() {
		JPanel panel1 = new JPanel(new GridLayout(2, 1));
		Container c = this.getContentPane();
		getButton.addActionListener(this);
		panel1.add(getButton);

		panel1.add(contentTable);
		c.add(panel1);
		panel1.setPreferredSize(new Dimension(400, 400));
		this.pack();
		this.show();
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

	}

	public static void main(String[] args) {

		new AppWindow();

		// System.out.println( "Hello World!" );
	}

	public void actionPerformed(ActionEvent arg0) {

		dbAccess = new DatabaseAccess();
		if (dbAccess.initConnection()) {
			
		}
		else{
			System.err.println("Connection error");
			
		}

	}
}

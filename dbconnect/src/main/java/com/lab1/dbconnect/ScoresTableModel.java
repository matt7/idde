package com.lab1.dbconnect;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javax.sql.rowset.CachedRowSet;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class ScoresTableModel implements TableModel {

	private CachedRowSet scoresRowSet;
	private ResultSetMetaData metadata;
	private int numcols, numrows;

	public ScoresTableModel(CachedRowSet rs) {
		this.scoresRowSet = rs;

		try {
			this.metadata = this.scoresRowSet.getMetaData();
			numcols = metadata.getColumnCount();

			// Retrieve the number of rows.
			this.scoresRowSet.beforeFirst();
			this.numrows = 0;
			while (this.scoresRowSet.next()) {
				this.numrows++;
			}
			this.scoresRowSet.beforeFirst();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void addTableModelListener(TableModelListener arg0) {
		// TODO Auto-generated method stub

	}

	public Class getColumnClass(int col) {
		// TODO Auto-generated method stub
		return String.class;
	}

	public int getColumnCount() {
		// TODO Auto-generated method stub
		return numcols;
	}

	public String getColumnName(int column) {
		// TODO Auto-generated method stub
		try {
			return this.metadata.getColumnLabel(column + 1);
		} catch (SQLException e) {
			return e.toString();
		}
	}

	public int getRowCount() {
		// TODO Auto-generated method stub
		return numrows;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		   try {
		        this.scoresRowSet.absolute(rowIndex + 1);
		        Object o = this.scoresRowSet.getObject(columnIndex + 1);
		        if (o == null)
		            return null;
		        else
		            return o.toString();
		    } catch (SQLException e) {
		        return e.toString();
		    }
	}

	public boolean isCellEditable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return false;
	}

	public void removeTableModelListener(TableModelListener arg0) {
		// TODO Auto-generated method stub

	}

	public void setValueAt(Object arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub

	}

}

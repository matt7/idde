package com.lab1.dbconnect;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;


/**
 * Hello world!
 * 
 */

public class DatabaseAccess {
	private String username = "root";
	private String password = "";
	private String server = "jdbc:mysql://localhost:";
	private String port="3306";
	private String dbName="kottafelismeres";
//jdbc:mysql://localhost:3306/
	private Connection conn = null;

	public DatabaseAccess() {

		
	}
	
	public boolean initConnection(){
		Properties connectionProps = new Properties();
		connectionProps.put("user", this.username);
		connectionProps.put("password", this.password);
		String constring=server+port+"/"+dbName;
		try {
			
			conn=DriverManager.getConnection(constring, connectionProps);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	

	

	
}

package dao.generic;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GenericJdbcDao<T> implements GenericDao<T> {

	private Class<T> persistentClass;

	public GenericJdbcDao(final Class<T> persistentClass) {
		this.persistentClass = persistentClass;
	}

	public GenericJdbcDao() {

	}

	public List<T> getAllDataRows() {
		String dbUrl = "jdbc:mysql://localhost:3306/labor1";
		String dbClass = "com.mysql.jdbc.Driver";
		String query = "Select * from "+persistentClass.getSimpleName();
		System.out.println(query);
		String username = "root";
		String password = "";
		try {
			Class.forName(dbClass);
			Connection connection = DriverManager.getConnection(dbUrl,
					username, password);
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(query);
			List<T> results = createObjects(resultSet);
			connection.close();
			return results;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	private List<T> createObjects(ResultSet resultSet)
			throws InstantiationException, IllegalAccessException,
			SecurityException, IllegalArgumentException,
			InvocationTargetException, SQLException, IntrospectionException {
		List<T> list = new ArrayList<T>();
		while (resultSet.next()) {
			T instance = persistentClass.newInstance();
			for (Field field : persistentClass.getDeclaredFields()) {
				Object value = resultSet.getObject(field.getName());
				PropertyDescriptor propertyDescriptor = new PropertyDescriptor(
						field.getName(), persistentClass);
				Method method = propertyDescriptor.getWriteMethod();
				if (field.getType() != java.lang.String.class) {
					method.invoke(instance, Integer.parseInt("" + value));
				} else {
					method.invoke(instance, value);
				}
			}
			list.add(instance);
		}
		return list;
	}

	public void update(T obj) {
		System.out.println("update");
		String dbUrl = "jdbc:mysql://localhost:3306/labor1";
		String dbClass = "com.mysql.jdbc.Driver";
		String query = "UPDATE "+persistentClass.getSimpleName()+" SET ";
		String username = "root";
		String password = "";
		try {
			Class.forName(dbClass);
			Connection connection = DriverManager.getConnection(dbUrl,
					username, password);
			boolean elso = true;
			for (Field field : persistentClass.getDeclaredFields()) {
				if(!field.getName().equals("id")){
					if (elso){
						query += field.getName()+"= ?";
						elso = false;
					}
					else{
						query += ", "+field.getName()+"=?";
					}					
				}
			}
			query += " where id=?;";
			System.out.println(query);
			PreparedStatement prep = connection.prepareStatement(query);
			int db = 1;
			int id = 0;
			for (Field field : persistentClass.getDeclaredFields()) {
				PropertyDescriptor propertyDescriptor = new PropertyDescriptor(
						field.getName(), persistentClass);
				Method method = propertyDescriptor.getReadMethod();
				if(!field.getName().equals("id")){
					if (field.getType() != java.lang.String.class) {
						int value = Integer.parseInt(method.invoke(obj).toString());
						prep.setInt(db, value);
					} else {
						prep.setString(db, method.invoke(obj).toString());
					}
					System.out.println(field.getName() + ": "
							+ method.invoke(obj).toString());
					db++;					
				}
				else{
					id = Integer.parseInt(method.invoke(obj).toString());
				}
			}
			prep.setInt(db, id);
			prep.executeUpdate();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void delete(T obj) {
		System.out.println("delete");
		String dbUrl = "jdbc:mysql://localhost:3306/labor1";
		String dbClass = "com.mysql.jdbc.Driver";
		String query = "DELETE FROM "+persistentClass.getSimpleName()+" WHERE ";
		String username = "root";
		String password = "";
		try {
			Class.forName(dbClass);
			Connection connection = DriverManager.getConnection(dbUrl,
					username, password);
			boolean elso = true;
			for (Field field : persistentClass.getDeclaredFields()) {
				if(!field.getName().equals("id")){
					if (elso){
						query += field.getName() + "= ? ";
						elso = false;
					}
					else{
						query += "and "+field.getName() + "= ? ";
					}					
				}
			}
			query += ";";
			System.out.println(query);
			PreparedStatement prep = connection.prepareStatement(query);

			int db = 1;
			for (Field field : persistentClass.getDeclaredFields()) {
				PropertyDescriptor propertyDescriptor = new PropertyDescriptor(
						field.getName(), persistentClass);
				Method method = propertyDescriptor.getReadMethod();
				if(!field.getName().equals("id")){
					if (field.getType() != java.lang.String.class) {
						int value = Integer.parseInt(method.invoke(obj).toString());
						prep.setInt(db, value);
					} else {
						prep.setString(db, method.invoke(obj).toString());
					}
					System.out.println(field.getName() + ": "
							+ method.invoke(obj).toString());
					db++;					
				}
			}
			prep.executeUpdate();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void insert(T obj) {
		System.out.println("insert");
		String dbUrl = "jdbc:mysql://localhost:3306/labor1";
		String dbClass = "com.mysql.jdbc.Driver";
		String query = "INSERT INTO "+persistentClass.getSimpleName()+" (";
		String username = "root";
		String password = "";
		try {
			Class.forName(dbClass);
			Connection connection = DriverManager.getConnection(dbUrl,
					username, password);
			boolean elso = true;
			String kerd = "";
			for (Field field : persistentClass.getDeclaredFields()) {
				if (elso){
					query += field.getName();
					kerd += " ?";
					elso = false;
				}
				else{
					query += ", "+field.getName();
					kerd += ", ?";
				}
			}
			query += ") VALUES ("+kerd+");";
			System.out.println(query);
			PreparedStatement prep = connection.prepareStatement(query);

			int db = 1;
			for (Field field : persistentClass.getDeclaredFields()) {
				PropertyDescriptor propertyDescriptor = new PropertyDescriptor(
						field.getName(), persistentClass);
				Method method = propertyDescriptor.getReadMethod();
				if (field.getType() != java.lang.String.class) {
					int value = Integer.parseInt(method.invoke(obj).toString());
					prep.setInt(db, value);
				} else {
					prep.setString(db, method.invoke(obj).toString());
				}
				System.out.println(field.getName() + ": "
						+ method.invoke(obj).toString());
				db++;
			}
			prep.executeUpdate();
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}

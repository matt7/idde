package dao.generic;

import java.util.List;

public interface GenericDao<T> {
	List<T> getAllDataRows();
	void insert(T obj);
	void update(T obj);
	void delete(T obj);
}

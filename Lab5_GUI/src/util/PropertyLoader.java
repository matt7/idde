package util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropertyLoader {
	protected static Properties database;
	protected static InputStream is;

	public static String getProperty(String key) {
		if (is == null) {
			is = PropertyLoader.class.getResourceAsStream("/database.properties");
		}
		if (database == null) {
			database = new Properties();
			try {
				database.load(is);
			} catch (IOException ex) {
				System.out.println("Error: "+ex);
			}
		}
		return database.getProperty(key);
	}
}

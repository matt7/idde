package lab5_gui.views;

import org.adatbazis.ABadatok;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

public class ViewCreate extends ViewPart {

	Composite myParent;
	TableViewer viewer;

	public void createPartControl(Composite parent) {
		myParent = parent;
		parent.setLayout(null);

		Label l = new Label(parent, SWT.PUSH);
		l.setText("PersonAge: ");
		l.setBounds(10, 10, 120, 20);
		l = new Label(parent, SWT.PUSH);
		l.setText("PersonName: ");
		l.setBounds(10, 40, 120, 20);
		l = new Label(parent, SWT.PUSH);
		l.setText("PersonAdress: ");
		l.setBounds(10, 70, 120, 20);
		l = new Label(parent, SWT.PUSH);
		l.setText("PersonPhoneNumber: ");
		l.setBounds(10, 100, 120, 20);

		final Text textAge = new Text(parent, SWT.PUSH);
		textAge.setBounds(150, 10, 200, 20);
		textAge.setText("21");
		textAge.setEditable(true);

		final Text textName = new Text(parent, SWT.PUSH);
		textName.setBounds(150, 40, 200, 20);
		textName.setText("Dezso");
		textName.setEditable(true);

		final Text textAdress = new Text(parent, SWT.PUSH);
		textAdress.setBounds(150, 70, 200, 20);
		textAdress.setText("Kolozsvaros");
		textAdress.setEditable(true);

		final Text textPhone = new Text(parent, SWT.PUSH);
		textPhone.setBounds(150, 100, 200, 20);
		textPhone.setText("0712345678");
		textPhone.setEditable(true);

		Button bt = new Button(parent, SWT.PUSH);
		bt.setText("Insert!");
		bt.setBounds(10, 130, 100, 30);

		final Composite btParent = parent;
		bt.addListener(SWT.Selection, new Listener() {

			public void handleEvent(Event event) {
				ABadatok ab = new ABadatok();
				ab.insert(Integer.parseInt(textAge.getText()),
						textName.getText(), textAdress.getText(),
						textPhone.getText());
				Label l = new Label(btParent, SWT.PUSH);
				l.setText("Inserted: " + textAge.getText() + "  "
						+ textName.getText() + "  " + textAdress.getText()
						+ "  " + textPhone.getText());
				l.setBounds(10, 170, 400, 20);

			}
		});

	}

	public void setFocus() {
	}

}

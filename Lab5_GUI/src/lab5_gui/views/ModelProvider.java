package lab5_gui.views;

import java.util.ArrayList;
import java.util.List;

import org.adatbazis.ABadatok;

import pojo.java.Person;

public class ModelProvider {
	private static ModelProvider INSTANCE;

	private List<Person> persons;

	private ModelProvider() {
		persons = new ArrayList<Person>();
		// Image here some fancy database access to read the persons and to
		// put them into the model
		// persons.add(new Person("Rainer", "Zufall", "male", true));
		// persons.add(new Person("Reiner", "Babbel", "male", true));
		// persons.add(new Person("Marie", "Dortmund", "female", false));
		// persons.add(new Person("Holger", "Adams", "male", true));
		// persons.add(new Person("Juliane", "Adams", "female", true));

	}

	public List<Person> getPersons() {
		persons = new ArrayList<Person>();
		ABadatok ab = new ABadatok();
		String data[][] = ab.getData();
		for (int i = 0; i < data.length; i++) {
			Person p = new Person(Integer.parseInt(data[i][0]),
					Integer.parseInt(data[i][1]), data[i][2], data[i][3],
					data[i][4]);
			persons.add(p);
		}
		return persons;
	}

	public static ModelProvider getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ModelProvider();
		}
		return INSTANCE;
	}

}

package lab5_gui.views;

import org.adatbazis.ABadatok;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import pojo.java.Person;

public class ViewDelete extends ViewPart {

	Composite myParent;
	TableViewer viewer;

	public void createPartControl(Composite parent) {
		myParent = parent;
		parent.setLayout(null);
		viewer = new TableViewer(parent, SWT.MULTI | SWT.H_SCROLL
				| SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

		createColumns(viewer);

		Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		table.setBounds(0, 0, 550, 200);

		Label l = new Label(parent, SWT.PUSH);
		l.setText("PersonAge: ");
		l.setBounds(0, 220, 120, 20);
		l = new Label(parent, SWT.PUSH);
		l.setText("PersonName: ");
		l.setBounds(0, 250, 120, 20);
		l = new Label(parent, SWT.PUSH);
		l.setText("PersonAdress: ");
		l.setBounds(0, 280, 120, 20);
		l = new Label(parent, SWT.PUSH);
		l.setText("PersonPhone: ");
		l.setBounds(0, 310, 120, 20);

		final Text textAge = new Text(parent, SWT.PUSH);
		textAge.setBounds(150, 220, 200, 20);
		textAge.setText("21");
		textAge.setEditable(true);

		final Text textName = new Text(parent, SWT.PUSH);
		textName.setBounds(150, 250, 200, 20);
		textName.setText("Dezso");
		textName.setEditable(true);

		final Text textAdress = new Text(parent, SWT.PUSH);
		textAdress.setBounds(150, 280, 200, 20);
		textAdress.setText("Kolozsvaros");
		textAdress.setEditable(true);

		final Text textPhone = new Text(parent, SWT.PUSH);
		textPhone.setBounds(150, 310, 200, 20);
		textPhone.setText("0712345678");
		textPhone.setEditable(true);

		Button bt = new Button(parent, SWT.PUSH);
		bt.setText("Delete!");
		bt.setBounds(50, 350, 100, 30);
		final Composite btParent = parent;
		bt.addListener(SWT.Selection, new Listener() {

			public void handleEvent(Event event) {
				ABadatok ab = new ABadatok();
				ab.delete(Integer.parseInt(textAge.getText()),
						textName.getText(), textAdress.getText(),
						textPhone.getText());
				Label l = new Label(btParent, SWT.PUSH);
				l.setText("Deleted: " + textName.getText());
				l.setBounds(10, 170, 400, 20);

			}
		});

	}

	private void createColumns(TableViewer viewer2) {
		
		ABadatok ab = new ABadatok();
		//String[] titles = { "First name", "Last name", "Gender" };
		String[] titles = ab.getColumns();
		int[] bounds = { 100, 100, 100, 100, 100 };

		TableViewerColumn col = createTableViewerColumn(titles[0], bounds[0], 0);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;
				return p.getId()+"";
			}
		});

		col = createTableViewerColumn(titles[1], bounds[1], 1);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;
				return p.getPersonAge()+"";
			}
		});

		col = createTableViewerColumn(titles[2], bounds[2], 2);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;
				return p.getPersonName();
			}
		});

		col = createTableViewerColumn(titles[3], bounds[3], 3);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;
				return p.getPersonAdress();
			}
		});
		
		col = createTableViewerColumn(titles[4], bounds[4], 4);
		col.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Person p = (Person) element;
				return p.getPersonPhoneNumber();
			}
		});

		viewer2.setContentProvider(new ArrayContentProvider());
		// get the content for the viewer, setInput will call getElements in the
		// contentProvider
		viewer2.setInput(ModelProvider.getInstance().getPersons());

	}

	private TableViewerColumn createTableViewerColumn(String title, int bound,
			final int colNumber) {
		final TableViewerColumn viewerColumn = new TableViewerColumn(viewer,
				SWT.NONE);
		final TableColumn column = viewerColumn.getColumn();
		column.setText(title);
		column.setWidth(bound);
		column.setResizable(true);
		column.setMoveable(true);
		return viewerColumn;
	}

	public void setFocus() {
	}

}

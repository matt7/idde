package pojo.java;

public class Person {

	protected int id;
	protected int personAge;
	protected String personName;
	protected String personAdress;
	protected String personPhoneNumber;
	
	public Person(){
	}

	public Person(int id, int personAge, String personName, String personAdress, String personPhoneNumber){
		this.id = id;
		this.personAdress = personAdress;
		this.personAge = personAge;
		this.personName = personName;
		this.personPhoneNumber = personPhoneNumber;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getPersonAge() {
		return personAge;
	}

	public void setPersonAge(int personAge) {
		this.personAge = personAge;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getPersonAdress() {
		return personAdress;
	}

	public void setPersonAdress(String personAdress) {
		this.personAdress = personAdress;
	}

	public String getPersonPhoneNumber() {
		return personPhoneNumber;
	}

	public void setPersonPhoneNumber(String personPhoneNumber) {
		this.personPhoneNumber = personPhoneNumber;
	}
	
	@Override
	public String toString() {
		return personName+":"+Integer.toString(personAge)+":"+personAdress+":"+personPhoneNumber;
	}
}

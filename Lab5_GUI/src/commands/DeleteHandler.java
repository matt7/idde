package commands;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.commands.IHandlerListener;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class DeleteHandler implements IHandler {

	public void addHandlerListener(IHandlerListener handlerListener) {		
	}

	public void dispose() {
	}

	public Object execute(ExecutionEvent event) throws ExecutionException {
		System.out.println("delete");
		IWorkbenchPage page = PlatformUI.getWorkbench()
				.getActiveWorkbenchWindow().getActivePage();
		try {
			page.showView("lab5_gui.views.ViewDelete").setFocus();
		} catch (PartInitException e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean isEnabled() {
		return true;
	}

	public boolean isHandled() {
		return true;
	}

	public void removeHandlerListener(IHandlerListener handlerListener) {
		
	}

}

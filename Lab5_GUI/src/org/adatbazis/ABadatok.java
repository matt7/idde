package org.adatbazis;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;

import pojo.java.Person;
import dao.generic.GenericJdbcDao;

public class ABadatok {
	static String data[][];
	static String columnNames[];

	public String[] getColumns() {
		System.out.println("getColumns:");
		
		GenericJdbcDao<Person> dao = new GenericJdbcDao<Person>(Person.class);
		List<Person> list = dao.getAllDataRows();
		Iterator<Person> it = list.listIterator();
		int col=0;
		if (it.hasNext()) {
			Person p = it.next();
			col = p.getClass().getDeclaredFields().length;
		}
		columnNames = new String[col];
		int dbcol = 0;
		it = list.listIterator();
		if (it.hasNext()) {
			Person p = it.next();
			dbcol = 0;
			for (Field f : p.getClass().getDeclaredFields()) {
				try {
					columnNames[dbcol++] = f.getName().toString();
					System.out.println(f.getName().toString());
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				}
			}
		}
		return columnNames;
	}

	public void updatedata(){
		GenericJdbcDao<Person> dao = new GenericJdbcDao<Person>(Person.class);
		Person p = new Person();
		p.setId(1);
		p.setPersonAge(22);
		p.setPersonName("Ali");
		p.setPersonAdress("Kolozsvaros");
		p.setPersonPhoneNumber("007007");
		dao.update(p);
	}
	
	public void update(int id, int age, String name, String adress, String phone){
		GenericJdbcDao<Person> dao = new GenericJdbcDao<Person>(Person.class);
		Person p = new Person();
		p.setId(id);
		p.setPersonAge(age);
		p.setPersonName(name);
		p.setPersonAdress(adress);
		p.setPersonPhoneNumber(phone);
		dao.update(p);
	}

	
	public void inserdata(){
		GenericJdbcDao<Person> dao = new GenericJdbcDao<Person>(Person.class);
		Person p = new Person();
		p.setPersonAge(21);
		p.setPersonName("Zsoltika");
		p.setPersonAdress("Kolozsvaros");
		p.setPersonPhoneNumber("0789456123");
		dao.insert(p);
	}

	public void insert(int age, String name, String adress, String phone){
		GenericJdbcDao<Person> dao = new GenericJdbcDao<Person>(Person.class);
		Person p = new Person();
		p.setPersonAge(age);
		p.setPersonName(name);
		p.setPersonAdress(adress);
		p.setPersonPhoneNumber(phone);
		dao.insert(p);
	}
	
	public void deletedata(){
		GenericJdbcDao<Person> dao = new GenericJdbcDao<Person>(Person.class);
		Person p = new Person();
		p.setPersonAge(21);
		p.setPersonName("Zsoltika");
		p.setPersonAdress("Kolozsvaros");
		p.setPersonPhoneNumber("0789456123");
		dao.delete(p);
	}
	
	public String[][] getData() {
		System.out.println("getData:");
		GenericJdbcDao<Person> dao = new GenericJdbcDao<Person>(Person.class);
		List<Person> list = dao.getAllDataRows();
		Iterator<Person> it = list.listIterator();
		int col = 0;
		int row = 0;
		while(it.hasNext()){
			Person p = it.next();
			row++;
			col = p.getClass().getDeclaredFields().length;
		}
		data = new String[row][col];
		System.out.println("row = " +row+ ", col = "+col);
		int dbrow = 0;
		int dbcol = 0;
		it = list.listIterator();
		while (it.hasNext()) {
			Person p = it.next();
			dbcol = 0;
			for (Field f : p.getClass().getDeclaredFields()) {
				PropertyDescriptor propertyDescriptor;
				try {
					propertyDescriptor = new PropertyDescriptor(f.getName(),
							p.getClass());
					Method method = propertyDescriptor.getReadMethod();
					System.out.println(f.getName() + "= " + method.invoke(p));
					data[dbrow][dbcol++] = method.invoke(p).toString();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (IntrospectionException e) {
					e.printStackTrace();
				}
			}
			dbrow++;
		}
		return data;
	}

	public void delete(int age, String name, String adress, String phone) {
		GenericJdbcDao<Person> dao = new GenericJdbcDao<Person>(Person.class);
		Person p = new Person();
		p.setPersonAge(age);
		p.setPersonName(name);
		p.setPersonAdress(adress);
		p.setPersonPhoneNumber(phone);
		dao.delete(p);
	}
	
}
